(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-registration-page-registration-page-module"],{

/***/ "1YN1":
/*!*********************************************************************!*\
  !*** ./src/app/pages/registration-page/registration-page.module.ts ***!
  \*********************************************************************/
/*! exports provided: RegistrationPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationPageModule", function() { return RegistrationPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "s7LF");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var _registration_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./registration.component */ "6nZn");





const routes = [
    {
        path: '',
        component: _registration_component__WEBPACK_IMPORTED_MODULE_4__["RegistrationComponent"]
    }
];
let RegistrationPageModule = class RegistrationPageModule {
};
RegistrationPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _registration_component__WEBPACK_IMPORTED_MODULE_4__["RegistrationComponent"]
        ],
        imports: [
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
        ],
        exports: []
    })
], RegistrationPageModule);

;


/***/ }),

/***/ "6nZn":
/*!*******************************************************************!*\
  !*** ./src/app/pages/registration-page/registration.component.ts ***!
  \*******************************************************************/
/*! exports provided: RegistrationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationComponent", function() { return RegistrationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_registration_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./registration.component.html */ "9WSS");
/* harmony import */ var _registration_component_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./registration.component.css */ "Gb2l");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "s7LF");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngxs/store */ "e1JD");
/* harmony import */ var src_app_auth_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/auth.service */ "ccyI");








let RegistrationComponent = class RegistrationComponent {
    constructor(Auth, store, router) {
        this.Auth = Auth;
        this.store = store;
        this.router = router;
    }
    ;
    ngOnInit() {
        this.formGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
            fname: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            lname: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            cpassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            dob: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            pob: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            nationality: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            number: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            pofemployment: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            profession: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            raffi: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            caffi: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            sattended: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            yeargraduated: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            degreeobtained: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            training: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            mstatus: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            fathern: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            fatherage: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            occupation: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            church: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            mothersname: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            mothersage: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            churcha: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            occup: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            once1: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            options: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            list: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            children: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            medication: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            hospital: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            understand: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            rites: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            fiance: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            together: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            live: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            marriage: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            mean: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            describe: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            different: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null),
            dateC: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null)
        });
    }
    ;
    submit() {
        const form = this.formGroup.value;
        console.log(form);
        this.Auth.registerUser(form).subscribe(res => {
            if (res.success) {
                return this.router.navigate(['Home']);
            }
            else {
                window.alert(res.message);
            }
        });
    }
};
RegistrationComponent.ctorParameters = () => [
    { type: src_app_auth_service__WEBPACK_IMPORTED_MODULE_7__["AuthService"] },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_6__["Store"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
];
RegistrationComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-registration',
        template: _raw_loader_registration_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_registration_component_css__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], RegistrationComponent);



/***/ }),

/***/ "9WSS":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/registration-page/registration.component.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <div class=\"registration-form\" [formGroup]=\"formGroup\">\r\n    <div style=\"\r\n        width: 500px;\r\n        margin-right: auto;\r\n        margin-left: auto;\r\n    \">\r\n        <h1>Personal Information</h1>\r\n        <div class=\"form-group\">\r\n            <input formControlName=\"fname\" type=\"text\" class=\"form-control item\" id=\"fname\" placeholder=\"First Name\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input  \r\n            formControlName=\"lname\"\r\n            type=\"text\" class=\"form-control item\" id=\"lname\" placeholder=\"Last Name\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"email\"\r\n            type=\"text\" class=\"form-control item\" id=\"email\" placeholder=\"Email\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"password\"\r\n            type=\"password\" class=\"form-control item\" id=\"password\" placeholder=\"Password\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"cpassword\"\r\n            type=\"password\" class=\"form-control item\" id=\"cpassword\" placeholder=\"Confirm Password\">\r\n        </div>\r\n        <label for=\"\">Date Of Birth</label>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"dob\"\r\n            type=\"date\" class=\"form-control item\" id=\"dob\" placeholder=\"Birth Date\">\r\n        </div>\r\n        \r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"pob\"\r\n            type=\"text\" class=\"form-control item\" id=\"pob\" placeholder=\"Place Of Birth\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"nationality\"\r\n            type=\"text\" class=\"form-control item\" id=\"nationality\" placeholder=\"Nationality\">\r\n        </div>\r\n       \r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"number\"\r\n            type=\"number\" class=\"form-control item\" id=\"number\" placeholder=\"Phone Number\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"pofemployment\"\r\n            type=\"text\" class=\"form-control item\" id=\"pofemployment\" placeholder=\"Place of Employment\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"profession\"\r\n            type=\"text\" class=\"form-control item\" id=\"profession\" placeholder=\"Profession\">\r\n        </div>\r\n      \r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"raffi\"\r\n            type=\"text\" class=\"form-control item\" id=\"raffi\" placeholder=\"Religious Affiliation\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"caffi\"\r\n            type=\"text\" class=\"form-control item\" id=\"caffi\" placeholder=\"If a Christian,What is your Church Affiliation\">\r\n        </div>\r\n\r\n\r\n        <h1>History of Education</h1>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"sattended\"\r\n            type=\"text\" class=\"form-control item\" id=\"sattended\" placeholder=\"School(s) Attended\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"yeargraduated\"\r\n            type=\"date\" class=\"form-control item\" id=\"yeargraduated\" placeholder=\"Year Graduated\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"degreeobtained\"\r\n            type=\"date\" class=\"form-control item\" id=\"degreeobtained\" placeholder=\"Degree(s) Obtained\">\r\n        </div>\r\n       \r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"training\"\r\n            type=\"text\" class=\"form-control item\" id=\"training\" placeholder=\"Training/Apprenticeship(if any)\">\r\n        </div>\r\n        <label for=\"marital\" >Marital Status</label>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"mstatus\"\r\n            type=\"text\" class=\"form-control item\" id=\"mstatus\" placeholder=\"Single ,Married Or Divorced\">\r\n        </div>\r\n\r\n\r\n\r\n        <h1>Parents</h1>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"fathern\"\r\n            type=\"text\" class=\"form-control item\" id=\"fathern\" placeholder=\"Father's Name\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"fatherage\"\r\n            type=\"text\" class=\"form-control item\" id=\"fatherage\" placeholder=\"Father's Age\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"occupation\"\r\n            type=\"text\" class=\"form-control item\" id=\"occupation\" placeholder=\"Occupation\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"church\"\r\n            type=\"text\" class=\"form-control item\" id=\"church\" placeholder=\"Church Affiliation\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"mothersname\"\r\n            type=\"text\" class=\"form-control item\" id=\"mothersname\" placeholder=\"Mother's Name\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"mothersage\"\r\n            type=\"text\" class=\"form-control item\" id=\"mothersage\" placeholder=\"Mother's Age\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"churcha\"\r\n            type=\"text\" class=\"form-control item\" id=\"churcha\" placeholder=\"Church Affiliation\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"occup\"\r\n            type=\"text\" class=\"form-control item\" id=\"occup\" placeholder=\"Occupation/Profession\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"once1\"\r\n            type=\"text\" class=\"form-control item\" id=\"once1\" placeholder=\"Have any of your parents been married more than once\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"options\"\r\n            type=\"text\" class=\"form-control item\" id=\"options\" placeholder=\"How were your parents married/options\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"list\"\r\n            type=\"text\" class=\"form-control item\" id=\"list\" placeholder=\"List your siblings,age,marital status and occupation\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"children\"\r\n            type=\"text\" class=\"form-control item\" id=\"children\" placeholder=\"Children's name,age,gender,marital status,occupation\">\r\n        </div>\r\n\r\n\r\n        <h1>Medical History</h1>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"medication\"\r\n            type=\"text\" class=\"form-control item\" id=\"medication\" placeholder=\"Are you currently taking any medication\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"hospital\"\r\n            type=\"text\" class=\"form-control item\" id=\"hospital\" placeholder=\"Have you ever been hospitalized and if yes for how long\">\r\n        </div>\r\n     cs\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"understand\"\r\n            type=\"text\" class=\"form-control item\" id=\"understand\" placeholder=\"What is your understanding of the role of a man in the house.\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input type=\"text\" class=\"form-control item\" id=\"rites\" placeholder=\"Was your marrigae rites performed under culture and traditon\">\r\n        </div>\r\n\r\n\r\n\r\n\r\n        <h1>For Premarital Counseling only</h1>\r\n\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"fiance\"\r\n            type=\"text\" class=\"form-control item\" id=\"fiance\" placeholder=\"How did you meet your fiance\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"together\"\r\n            type=\"text\" class=\"form-control item\" id=\"together\" placeholder=\"How do you know that both of you are meant to be together\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"live\"\r\n            type=\"text\" class=\"form-control item\" id=\"live\" placeholder=\"Do you live with your finace\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"marriage\"\r\n            type=\"text\" class=\"form-control item\" id=\"marriage\" placeholder=\"What do you see marriage as?\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"mean\"\r\n            type=\"text\" class=\"form-control item\" id=\"mean\" placeholder=\"What does marriage mean to you\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"describe\"\r\n            type=\"text\" class=\"form-control item\" id=\"describe\" placeholder=\"How similar is your parent's marriage to the marriage you described above\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"different\"\r\n            type=\"text\" class=\"form-control item\" id=\"different\" placeholder=\"In what ways will your marriage be different from or similar to your marriage\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input \r\n            formControlName=\"dateC\"\r\n            type=\"date\" class=\"form-control item\" id=\"dateC\" placeholder=\"Birth Date\">\r\n        </div>\r\n\r\n        <div class=\"form-group\">\r\n            <button\r\n                class=\"btn btn-primary\"\r\n                type=\"submit\" \r\n                id=\"submit\" \r\n                (click)=\"submit()\">Done</button>\r\n        </div>\r\n    </div>\r\n</div> -->\r\n\r\n<div class=\"container\" [formGroup]=\"formGroup\">\r\n    <div class=\"col-md-6\" id=\"form\">\r\n <form>\r\n<h1 class=\"title\">Registration</h1>\r\n<br><br> \r\n\r\n    <div class=\"container\">\r\n        <div class=\"row\" >\r\n        <h1>Personal Information</h1>\r\n        <div class=\"col-md-1\"></div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">First Name</span>\r\n            <input  type=\"text\" class=\"form-control item\" id=\"fname\" placeholder=\"First Name\">\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">Last  Name</span>\r\n            <input  \r\n            formControlName=\"lname\"\r\n            type=\"text\" class=\"form-control item\" id=\"lname\" placeholder=\"Last Name\">\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">Email</span>\r\n            <input \r\n            formControlName=\"email\"\r\n            type=\"text\" class=\"form-control item\" id=\"email\" placeholder=\"Email\">\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">Password</span>\r\n            <input \r\n            formControlName=\"password\"\r\n            type=\"password\" class=\"form-control item\" id=\"password\" placeholder=\"Password\">\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">Confirm Password</span>\r\n            <input \r\n            formControlName=\"cpassword\"\r\n            type=\"password\" class=\"form-control item\" id=\"cpassword\" placeholder=\"Confirm Password\">\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">Birth Date</span>\r\n            <input \r\n            formControlName=\"dob\"\r\n            type=\"date\" class=\"form-control item\" id=\"dob\" placeholder=\"Birth Date\">\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">Place Of Birth</span>\r\n            <input \r\n            formControlName=\"pob\"\r\n            type=\"text\" class=\"form-control item\" id=\"pob\" placeholder=\"Place Of Birth\">\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">Nationality</span>\r\n            <input \r\n            formControlName=\"nationality\"\r\n            type=\"text\" class=\"form-control item\" id=\"nationality\" placeholder=\"Nationality\">\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">Phone Number</span>\r\n            <input \r\n            formControlName=\"number\"\r\n            type=\"number\" class=\"form-control item\" id=\"number\" placeholder=\"Phone Number\">\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">Place Of Employment</span>\r\n            <input \r\n            formControlName=\"pofemployment\"\r\n            type=\"text\" class=\"form-control item\" id=\"pofemployment\" placeholder=\"Place of Employment\">\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">Profession</span>\r\n            <input \r\n            formControlName=\"profession\"\r\n            type=\"text\" class=\"form-control item\" id=\"profession\" placeholder=\"Profession\">\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">Religious Affiliation</span>\r\n            <input \r\n            formControlName=\"raffi\"\r\n            type=\"text\" class=\"form-control item\" id=\"raffi\" placeholder=\"Religious Affiliation\">\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">  If a Christian,What is your Church Affiliation</span>\r\n            <input \r\n            formControlName=\"caffi\"\r\n            type=\"text\" class=\"form-control item\" id=\"caffi\" placeholder=\"If a Christian,What is your Church Affiliation\">\r\n        </div>\r\n\r\n        <h1>History Of Education</h1>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">Highest Degree Obtained</span>\r\n            <input \r\n            formControlName=\"sattended\"\r\n            type=\"text\" class=\"form-control item\" id=\"sattended\" placeholder=\"School(s) Attended\">\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\"> Year Graduated</span>\r\n            <input \r\n            formControlName=\"yeargraduated\"\r\n            type=\"date\" class=\"form-control item\" id=\"yeargraduated\" placeholder=\"Year Graduated\">\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">Degree Obtained </span>\r\n            <input \r\n            formControlName=\"degreeobtained\"\r\n            type=\"date\" class=\"form-control item\" id=\"degreeobtained\" placeholder=\"Degree(s) Obtained\">\r\n\r\n        </div>\r\n        \r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\"> Training/Apprenticeship</span>\r\n            <input \r\n            formControlName=\"training\"\r\n            type=\"text\" class=\"form-control item\" id=\"training\" placeholder=\"Training/Apprenticeship(if any)\">\r\n\r\n        </div>\r\n        <h1>Marital Status</h1>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">Single,Married Or Divorced</span>\r\n            <input \r\n            formControlName=\"mstatus\"\r\n            type=\"text\" class=\"form-control item\" id=\"mstatus\" placeholder=\"Single ,Married Or Divorced\">\r\n\r\n        </div>\r\n        <!-- <div class=\"col-md-5\">\r\n            <span class=\"details\"> How many times have you been married?</span>\r\n            <input type=\"text\" placeholder=\"# times\">\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">How did the previous end?</span>\r\n            <input type=\"text\" placeholder=\"Death,Divorce,Abandonment,Separation\">\r\n\r\n        </div> -->\r\n\r\n        <h1>Parents</h1>\r\n        <br>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">  Father</span>\r\n            <input \r\n            formControlName=\"fathern\"\r\n            type=\"text\" class=\"form-control item\" id=\"fathern\" placeholder=\"Father's Name\">\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\"> Father's Age</span>\r\n            <input \r\n            formControlName=\"fatherage\"\r\n            type=\"text\" class=\"form-control item\" id=\"fatherage\" placeholder=\"Father's Age\">\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\"> Occupation</span>\r\n         <input \r\n            formControlName=\"occupation\"\r\n            type=\"text\" class=\"form-control item\" id=\"occupation\" placeholder=\"Occupation\">\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">Church Affiliation</span>\r\n            <input \r\n            formControlName=\"church\"\r\n            type=\"text\" class=\"form-control item\" id=\"church\" placeholder=\"Church Affiliation\">\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">Mother's Name</span>\r\n            <input \r\n            formControlName=\"mothersname\"\r\n            type=\"text\" class=\"form-control item\" id=\"mothersname\" placeholder=\"Mother's Name\">\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">Mother's Age</span>\r\n            <input \r\n            formControlName=\"mothersage\"\r\n            type=\"text\" class=\"form-control item\" id=\"mothersage\" placeholder=\"Mother's Age\">\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">Mother's Occupation</span>\r\n            <input \r\n            formControlName=\"occup\"\r\n            type=\"text\" class=\"form-control item\" id=\"occup\" placeholder=\"Occupation/Profession\">\r\n        </div>\r\n        \r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">  Church Affiliation</span>\r\n            <input \r\n            formControlName=\"churcha\"\r\n            type=\"text\" class=\"form-control item\" id=\"churcha\" placeholder=\"Church Affiliation\">\r\n        </div>\r\n\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\"> How many times have your parents been married</span>\r\n            <input \r\n            formControlName=\"once1\"\r\n            type=\"text\" class=\"form-control item\" id=\"once1\" placeholder=\"No. Of Times \">\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">Was your parents marriage in ..</span>\r\n            <input \r\n            formControlName=\"options\"\r\n            type=\"text\" class=\"form-control item\" id=\"options\" placeholder=\"Church, Court Or Traditional Marriage\">\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\"> Brothers and Sisters</span>\r\n            <input \r\n            formControlName=\"list\"\r\n            type=\"text\" class=\"form-control item\" id=\"list\" placeholder=\"List your Siblings\">\r\n\r\n        </div>\r\n        <br>\r\n\r\n        <h1>Children</h1>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">Names</span>\r\n  <input \r\n            formControlName=\"children\"\r\n            type=\"text\" class=\"form-control item\" id=\"children\" placeholder=\"List your Children,Age,Sex,Marital Status,Occupation\">\r\n        </div>\r\n\r\n        <!-- <div class=\"col-md-5\">\r\n            <span class=\"details\"> Are you currently employed</span>\r\n            <input type=\"text\" placeholder=\"Employment\">\r\n        </div>\r\n\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\"> What is the longest you have been on your job?</span>\r\n            <input type=\"text\" placeholder=\"Year Range\">\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">How many jobs have you held over the last three years</span>\r\n            <input type=\"text\" placeholder=\"Number Of Jobs\">\r\n\r\n        </div> -->\r\n\r\n        <h1>Medical History</h1>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">Are you currently taking any medication</span>\r\n            <input \r\n            formControlName=\"medication\"\r\n            type=\"text\" class=\"form-control item\" id=\"medication\" placeholder=\"List of Medication\">\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\"> Have you ever been hosptalized and if yes for how long</span>\r\n            <input \r\n            formControlName=\"hospital\"\r\n            type=\"text\" class=\"form-control item\" id=\"hospital\" placeholder=\"Yes Or No \">\r\n\r\n        </div>\r\n\r\n        <h1>Doctrinal Information</h1>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">  What is your understanding of the role of a man in the house.</span>\r\n            <input \r\n            formControlName=\"understand\"\r\n            type=\"text\" class=\"form-control item\" id=\"understand\" placeholder= \"The Role\" >\r\n\r\n        </div>\r\n       \r\n        <br>\r\n        <h1>Cultural And Traditional Information</h1>\r\n        <br>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\"> Was your marrigae rites performed under culture and traditon</span>\r\n            <input type=\"text\" class=\"form-control item\" id=\"rites\" placeholder=\"Yes Or No\">\r\n\r\n        </div>\r\n        <br>\r\n\r\n\r\n\r\n        <h1>For Premarital Counseling only</h1>\r\n        <br>\r\n\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\"> How did you meet your fiance</span>\r\n            <input \r\n            formControlName=\"fiance\"\r\n            type=\"text\" class=\"form-control item\" id=\"fiance\" placeholder= \"Please enter here\" >\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">  How do you know that both of you are meant to be together</span>\r\n            <input \r\n            formControlName=\"together\"\r\n            type=\"text\" class=\"form-control item\" id=\"together\" placeholder= \"How would you know\" >\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">Do you live with your fiance</span>\r\n            <input \r\n            formControlName=\"live\"\r\n            type=\"text\" class=\"form-control item\" id=\"live\" placeholder= \"How would you know\" >\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">What do you see marriage as?</span>\r\n            <input \r\n            formControlName=\"marriage\"\r\n            type=\"text\" class=\"form-control item\" id=\"marriage\"  placeholder= \"What do you know\" >\r\n\r\n        </div>\r\n\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">What does marriage mean to you</span>\r\n            <input \r\n            formControlName=\"mean\"\r\n            type=\"text\" class=\"form-control item\" id=\"mean\" placeholder= \"Explain here\" >\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">How similar is your parent's marriage to the marriage you described above</span>\r\n            <input \r\n            formControlName=\"describe\"\r\n            type=\"text\" class=\"form-control item\" id=\"describe\" placeholder= \"How would you know\" >\r\n\r\n        </div>\r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">In what ways will your marriage be different from or similar to your marriage</span>\r\n            <input \r\n            formControlName=\"different\"\r\n            type=\"text\" class=\"form-control item\" id=\"different\"  placeholder= \"How would you know\" >\r\n\r\n        </div>\r\n\r\n    \r\n        <div class=\"col-md-5\">\r\n            <span class=\"details\">Date of Completion</span>\r\n            <input \r\n            formControlName=\"dateC\"\r\n            type=\"date\" class=\"form-control item\" id=\"dateC\" placeholder=\"Today's Date\">\r\n\r\n        </div>\r\n        <br>\r\n        <div class=\"form-group\">\r\n            <button\r\n                class=\"btn btn-primary\"\r\n                type=\"submit\" \r\n                id=\"submit\" \r\n                (click)=\"submit()\">Done</button>\r\n        </div>\r\n        </div>\r\n    </div>\r\n    </form>\r\n</div>\r\n</div>\r\n");

/***/ }),

/***/ "Gb2l":
/*!********************************************************************!*\
  !*** ./src/app/pages/registration-page/registration.component.css ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* body {\r\n    background-color: #dee9ff;\r\n}\r\n\r\n.registration-form {\r\n    padding: 50px 0;\r\n}\r\n\r\n.registration-form form {\r\n    background-color: #fff;\r\n    max-width: 600px;\r\n    margin: auto;\r\n    padding: 50px 70px;\r\n    border-top-left-radius: 30px;\r\n    border-top-right-radius: 30px;\r\n    box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.075);\r\n}\r\n\r\n.registration-form .form-icon {\r\n    text-align: center;\r\n    background-color: #5891ff;\r\n    border-radius: 50%;\r\n    font-size: 40px;\r\n    color: white;\r\n    width: 100px;\r\n    height: 100px;\r\n    margin: auto;\r\n    margin-bottom: 50px;\r\n    line-height: 100px;\r\n}\r\n\r\n.registration-form .item {\r\n    border-radius: 20px;\r\n    margin-bottom: 25px;\r\n    padding: 10px 20px;\r\n}\r\n\r\n.registration-form .create-account {\r\n    border-radius: 30px;\r\n    padding: 10px 20px;\r\n    font-size: 18px;\r\n    font-weight: bold;\r\n    background-color: #5791ff;\r\n    border: none;\r\n    color: white;\r\n    margin-top: 20px;\r\n}\r\n\r\n.registration-form .social-media {\r\n    max-width: 600px;\r\n    background-color: #fff;\r\n    margin: auto;\r\n    padding: 35px 0;\r\n    text-align: center;\r\n    border-bottom-left-radius: 30px;\r\n    border-bottom-right-radius: 30px;\r\n    color: #9fadca;\r\n    border-top: 1px solid #dee9ff;\r\n    box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.075);\r\n}\r\n\r\n.registration-form .social-icons {\r\n    margin-top: 30px;\r\n    margin-bottom: 16px;\r\n}\r\n\r\n.registration-form .social-icons a {\r\n    font-size: 23px;\r\n    margin: 0 3px;\r\n    color: #5691ff;\r\n    border: 1px solid;\r\n    border-radius: 50%;\r\n    width: 45px;\r\n    display: inline-block;\r\n    height: 45px;\r\n    text-align: center;\r\n    background-color: #fff;\r\n    line-height: 45px;\r\n}\r\n\r\n.registration-form .social-icons a:hover {\r\n    text-decoration: none;\r\n    opacity: 0.6;\r\n}\r\n\r\n@media (max-width: 576px) {\r\n    .registration-form form {\r\n        padding: 50px 20px;\r\n    }\r\n    .registration-form .form-icon {\r\n        width: 70px;\r\n        height: 70px;\r\n        font-size: 30px;\r\n        line-height: 70px;\r\n    }\r\n} */\r\n\r\n\r\n\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlZ2lzdHJhdGlvbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQStGRyIsImZpbGUiOiJyZWdpc3RyYXRpb24uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIGJvZHkge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2RlZTlmZjtcclxufVxyXG5cclxuLnJlZ2lzdHJhdGlvbi1mb3JtIHtcclxuICAgIHBhZGRpbmc6IDUwcHggMDtcclxufVxyXG5cclxuLnJlZ2lzdHJhdGlvbi1mb3JtIGZvcm0ge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxuICAgIG1heC13aWR0aDogNjAwcHg7XHJcbiAgICBtYXJnaW46IGF1dG87XHJcbiAgICBwYWRkaW5nOiA1MHB4IDcwcHg7XHJcbiAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAzMHB4O1xyXG4gICAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDMwcHg7XHJcbiAgICBib3gtc2hhZG93OiAwcHggMnB4IDEwcHggcmdiYSgwLCAwLCAwLCAwLjA3NSk7XHJcbn1cclxuXHJcbi5yZWdpc3RyYXRpb24tZm9ybSAuZm9ybS1pY29uIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM1ODkxZmY7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBmb250LXNpemU6IDQwcHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB3aWR0aDogMTAwcHg7XHJcbiAgICBoZWlnaHQ6IDEwMHB4O1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNTBweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxMDBweDtcclxufVxyXG5cclxuLnJlZ2lzdHJhdGlvbi1mb3JtIC5pdGVtIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyNXB4O1xyXG4gICAgcGFkZGluZzogMTBweCAyMHB4O1xyXG59XHJcblxyXG4ucmVnaXN0cmF0aW9uLWZvcm0gLmNyZWF0ZS1hY2NvdW50IHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbiAgICBwYWRkaW5nOiAxMHB4IDIwcHg7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM1NzkxZmY7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG59XHJcblxyXG4ucmVnaXN0cmF0aW9uLWZvcm0gLnNvY2lhbC1tZWRpYSB7XHJcbiAgICBtYXgtd2lkdGg6IDYwMHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIHBhZGRpbmc6IDM1cHggMDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDMwcHg7XHJcbiAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMzBweDtcclxuICAgIGNvbG9yOiAjOWZhZGNhO1xyXG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNkZWU5ZmY7XHJcbiAgICBib3gtc2hhZG93OiAwcHggMnB4IDEwcHggcmdiYSgwLCAwLCAwLCAwLjA3NSk7XHJcbn1cclxuXHJcbi5yZWdpc3RyYXRpb24tZm9ybSAuc29jaWFsLWljb25zIHtcclxuICAgIG1hcmdpbi10b3A6IDMwcHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxNnB4O1xyXG59XHJcblxyXG4ucmVnaXN0cmF0aW9uLWZvcm0gLnNvY2lhbC1pY29ucyBhIHtcclxuICAgIGZvbnQtc2l6ZTogMjNweDtcclxuICAgIG1hcmdpbjogMCAzcHg7XHJcbiAgICBjb2xvcjogIzU2OTFmZjtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgd2lkdGg6IDQ1cHg7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBoZWlnaHQ6IDQ1cHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xyXG4gICAgbGluZS1oZWlnaHQ6IDQ1cHg7XHJcbn1cclxuXHJcbi5yZWdpc3RyYXRpb24tZm9ybSAuc29jaWFsLWljb25zIGE6aG92ZXIge1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgb3BhY2l0eTogMC42O1xyXG59XHJcblxyXG5AbWVkaWEgKG1heC13aWR0aDogNTc2cHgpIHtcclxuICAgIC5yZWdpc3RyYXRpb24tZm9ybSBmb3JtIHtcclxuICAgICAgICBwYWRkaW5nOiA1MHB4IDIwcHg7XHJcbiAgICB9XHJcbiAgICAucmVnaXN0cmF0aW9uLWZvcm0gLmZvcm0taWNvbiB7XHJcbiAgICAgICAgd2lkdGg6IDcwcHg7XHJcbiAgICAgICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgICAgICBsaW5lLWhlaWdodDogNzBweDtcclxuICAgIH1cclxufSAqL1xyXG5cclxuXHJcblxyXG5cclxuIl19 */");

/***/ }),

/***/ "ccyI":
/*!*********************************!*\
  !*** ./src/app/auth.service.ts ***!
  \*********************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "IheW");
/* harmony import */ var _api_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./api/api */ "lL+R");




let AuthService = class AuthService {
    constructor(http) {
        this.http = http;
        this.loggedInStatus = false;
        this.registerStatus = false;
    }
    setLoggedIn(value) {
        this.loggedInStatus = value;
    }
    get isLoggedIn() {
        return this.loggedInStatus;
    }
    setRegister(value) {
        this.registerStatus = value;
    }
    get isRegister() {
        return this.registerStatus;
    }
    getUserDetails(email, password) {
        return this.http.post(`${_api_api__WEBPACK_IMPORTED_MODULE_3__["default"]}/login`, {
            email: email,
            password: password
        });
    }
    registerUser(data) {
        return this.http.post(`${_api_api__WEBPACK_IMPORTED_MODULE_3__["default"]}/register`, data);
    }
    ;
    getOTP(otp) {
        return this.http.post(`${_api_api__WEBPACK_IMPORTED_MODULE_3__["default"]}/verify`, {
            otp: otp
        });
    }
    summary() {
        return this.http.get(`${_api_api__WEBPACK_IMPORTED_MODULE_3__["default"]}/summary/get`);
    }
};
AuthService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
AuthService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], AuthService);

;


/***/ })

}]);
//# sourceMappingURL=pages-registration-page-registration-page-module.js.map