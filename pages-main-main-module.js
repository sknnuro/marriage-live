(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-main-main-module"],{

/***/ "/s1f":
/*!**********************************************!*\
  !*** ./src/app/pages/main/main.component.ts ***!
  \**********************************************/
/*! exports provided: MainComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainComponent", function() { return MainComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_main_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./main.component.html */ "bBbC");
/* harmony import */ var _main_component_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./main.component.css */ "piB7");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngxs/store */ "e1JD");
/* harmony import */ var src_app_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/auth.service */ "ccyI");
/* harmony import */ var src_app_http_http_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/http/http.service */ "RjLs");
/* harmony import */ var src_app_store_store_actions__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/store/store.actions */ "kPKD");









let MainComponent = class MainComponent {
    constructor(Auth, router, http, store) {
        this.Auth = Auth;
        this.router = router;
        this.http = http;
        this.store = store;
    }
    ngOnInit() {
    }
    loginUser(event) {
        event.preventDefault();
        const target = event.target;
        const email = target.querySelector("#email").value;
        const password = target.querySelector("#password").value;
        if (email !== 'admin@gmail.com' && password !== '@admin12') {
            return;
        }
        return this.store.dispatch(new src_app_store_store_actions__WEBPACK_IMPORTED_MODULE_8__["PushState"]({
            admin: {
                email: email,
                password: password
            }
        })).subscribe(() => {
            console.log('Hello...');
            this.router.navigate(['dashboard']);
        });
        // this.Auth.getUserDetails(email,password).subscribe(data=>{
        //   if (data.success){
        //     this.router.navigate(['dashboard'])
        //     this.Auth.setLoggedIn(true)
        //   }
        // })
    }
    loginUser1(event) {
        event.preventDefault();
        const target = event.target;
        const email = target.querySelector("#email").value;
        const password = target.querySelector("#password").value;
        this.Auth.getUserDetails(email, password).subscribe(data => {
            if (data.success) {
                this.router.navigate(['verification']);
                this.Auth.setLoggedIn(true);
            }
        });
    }
};
MainComponent.ctorParameters = () => [
    { type: src_app_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_http_http_service__WEBPACK_IMPORTED_MODULE_7__["HttpService"] },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Store"] }
];
MainComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-main',
        template: _raw_loader_main_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_main_component_css__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], MainComponent);



/***/ }),

/***/ "3swW":
/*!*****************************************************!*\
  !*** ./src/app/pages/main/login/login.component.ts ***!
  \*****************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_login_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./login.component.html */ "ppKA");
/* harmony import */ var _login_component_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login.component.css */ "HuzN");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var src_app_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/auth.service */ "ccyI");
/* harmony import */ var src_app_http_http_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/http/http.service */ "RjLs");







let LoginComponent = class LoginComponent {
    constructor(Auth, router, http) {
        this.Auth = Auth;
        this.router = router;
        this.http = http;
    }
    ngOnInit() {
    }
    loginUser1(event) {
        event.preventDefault();
        const target = event.target;
        const email = target.querySelector("#email").value;
        const password = target.querySelector("#password").value;
        this.Auth.getUserDetails(email, password).subscribe(data => {
            if (data.success) {
                this.router.navigate(['Home/verification']);
                this.Auth.setLoggedIn(true);
            }
        });
    }
};
LoginComponent.ctorParameters = () => [
    { type: src_app_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_http_http_service__WEBPACK_IMPORTED_MODULE_6__["HttpService"] }
];
LoginComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-login',
        template: _raw_loader_login_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_login_component_css__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], LoginComponent);



/***/ }),

/***/ "82nU":
/*!*******************************************!*\
  !*** ./src/app/pages/main/main.module.ts ***!
  \*******************************************/
/*! exports provided: MainRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainRoutingModule", function() { return MainRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "SVse");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var src_app_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/auth.service */ "ccyI");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/components/components.module */ "j1ZV");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login/login.component */ "3swW");
/* harmony import */ var _main_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./main.component */ "/s1f");
/* harmony import */ var _personality_personality_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./personality/personality.component */ "lSLK");
/* harmony import */ var _verification_verification_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./verification/verification.component */ "tz80");










const routes = [
    {
        path: '',
        component: _main_component__WEBPACK_IMPORTED_MODULE_7__["MainComponent"],
        children: [
            { path: "Login", component: _login_login_component__WEBPACK_IMPORTED_MODULE_6__["LoginComponent"] },
            { path: "Personality", component: _personality_personality_component__WEBPACK_IMPORTED_MODULE_8__["PersonalityComponent"] },
            { path: "verification", component: _verification_verification_component__WEBPACK_IMPORTED_MODULE_9__["VerificationComponent"] },
        ]
    },
];
let MainRoutingModule = class MainRoutingModule {
};
MainRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _main_component__WEBPACK_IMPORTED_MODULE_7__["MainComponent"],
            _login_login_component__WEBPACK_IMPORTED_MODULE_6__["LoginComponent"],
            _personality_personality_component__WEBPACK_IMPORTED_MODULE_8__["PersonalityComponent"],
            _verification_verification_component__WEBPACK_IMPORTED_MODULE_9__["VerificationComponent"],
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_5__["ComponentsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
        ],
        providers: [
            src_app_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]
        ],
        exports: []
    })
], MainRoutingModule);



/***/ }),

/***/ "HuzN":
/*!******************************************************!*\
  !*** ./src/app/pages/main/login/login.component.css ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".form-container{\r\n    position: absolute;\r\n    top: 50%;\r\n    left: 50%;\r\n    transform:translate(-50%,-50%);\r\n    background: linear-gradient(rgba(0,0,0,0.3),rgba(0,0,0,0.3));\r\n    width :380px;\r\n    padding:50px 30px;\r\n    border-radius :10px;\r\n    box-shadow: 7px 5px 60px #000;\r\n}\r\nh1{\r\n    text-transform: uppercase;\r\n    font-size :2em;\r\n    text-align :center;\r\n    margin-bottom : 0em ; \r\n    color: white;\r\n\r\n}\r\n.control input{\r\n    width:100%;\r\n    display:block;\r\n    padding :10px;\r\n    color: #000;\r\n    border:none;\r\n    outline:none;\r\n    margin: 1em 0 ;\r\n    border-radius: 20px; \r\n\r\n}\r\n.form-label{\r\n    color:white;\r\n\r\n}\r\nbutton{\r\n    width: 60%;\r\n    background: purple;\r\n    color: white;\r\n    border-radius: 20px;\r\n    height: 40px;\r\n}\r\nbutton:hover{\r\n    background: rgb(94, 94, 212);\r\n    transition: 0.5s;\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvZ2luLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxrQkFBa0I7SUFDbEIsUUFBUTtJQUNSLFNBQVM7SUFDVCw4QkFBOEI7SUFDOUIsNERBQTREO0lBQzVELFlBQVk7SUFDWixpQkFBaUI7SUFDakIsbUJBQW1CO0lBQ25CLDZCQUE2QjtBQUNqQztBQUNBO0lBQ0kseUJBQXlCO0lBQ3pCLGNBQWM7SUFDZCxrQkFBa0I7SUFDbEIsb0JBQW9CO0lBQ3BCLFlBQVk7O0FBRWhCO0FBRUE7SUFDSSxVQUFVO0lBQ1YsYUFBYTtJQUNiLGFBQWE7SUFDYixXQUFXO0lBQ1gsV0FBVztJQUNYLFlBQVk7SUFDWixjQUFjO0lBQ2QsbUJBQW1COztBQUV2QjtBQUNBO0lBQ0ksV0FBVzs7QUFFZjtBQUVBO0lBQ0ksVUFBVTtJQUNWLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLFlBQVk7QUFDaEI7QUFDQTtJQUNJLDRCQUE0QjtJQUM1QixnQkFBZ0I7QUFDcEIiLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mb3JtLWNvbnRhaW5lcntcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogNTAlO1xyXG4gICAgbGVmdDogNTAlO1xyXG4gICAgdHJhbnNmb3JtOnRyYW5zbGF0ZSgtNTAlLC01MCUpO1xyXG4gICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHJnYmEoMCwwLDAsMC4zKSxyZ2JhKDAsMCwwLDAuMykpO1xyXG4gICAgd2lkdGggOjM4MHB4O1xyXG4gICAgcGFkZGluZzo1MHB4IDMwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzIDoxMHB4O1xyXG4gICAgYm94LXNoYWRvdzogN3B4IDVweCA2MHB4ICMwMDA7XHJcbn1cclxuaDF7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgZm9udC1zaXplIDoyZW07XHJcbiAgICB0ZXh0LWFsaWduIDpjZW50ZXI7XHJcbiAgICBtYXJnaW4tYm90dG9tIDogMGVtIDsgXHJcbiAgICBjb2xvcjogd2hpdGU7XHJcblxyXG59XHJcblxyXG4uY29udHJvbCBpbnB1dHtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBkaXNwbGF5OmJsb2NrO1xyXG4gICAgcGFkZGluZyA6MTBweDtcclxuICAgIGNvbG9yOiAjMDAwO1xyXG4gICAgYm9yZGVyOm5vbmU7XHJcbiAgICBvdXRsaW5lOm5vbmU7XHJcbiAgICBtYXJnaW46IDFlbSAwIDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7IFxyXG5cclxufVxyXG4uZm9ybS1sYWJlbHtcclxuICAgIGNvbG9yOndoaXRlO1xyXG5cclxufVxyXG5cclxuYnV0dG9ue1xyXG4gICAgd2lkdGg6IDYwJTtcclxuICAgIGJhY2tncm91bmQ6IHB1cnBsZTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbn1cclxuYnV0dG9uOmhvdmVye1xyXG4gICAgYmFja2dyb3VuZDogcmdiKDk0LCA5NCwgMjEyKTtcclxuICAgIHRyYW5zaXRpb246IDAuNXM7XHJcbn1cclxuXHJcbiJdfQ== */");

/***/ }),

/***/ "N3D8":
/*!******************************************************************!*\
  !*** ./src/app/pages/main/personality/personality.component.css ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".form-container{\r\n    position: absolute;\r\n    top: 50%;\r\n    left: 50%;\r\n    transform:translate(-50%,-50%);\r\n    background: linear-gradient(rgba(0,0,0,0.3),rgba(0,0,0,0.3));\r\n    width :380px;\r\n    padding:50px 30px;\r\n    border-radius :10px;\r\n    box-shadow: 7px 5px 60px #000;\r\n}\r\nh1{\r\n    text-transform: uppercase;\r\n    font-size :2em;\r\n    text-align :center;\r\n    margin-bottom : 0em ; \r\n    color: white;\r\n\r\n}\r\n.control input{\r\n    width:100%;\r\n    display:block;\r\n    padding :10px;\r\n    color: #000;\r\n    border:none;\r\n    outline:none;\r\n    margin: 1em 0 ;\r\n    border-radius: 20px; \r\n\r\n}\r\n.form-label{\r\n    color:white;\r\n\r\n}\r\nbutton{\r\n    width: 60%;\r\n    background: purple;\r\n    color: white;\r\n    border-radius: 20px;\r\n    height: 40px;\r\n    margin-top: 20px;\r\n}\r\nbutton:hover{\r\n    background: rgb(94, 94, 212);\r\n    transition: 0.5s;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBlcnNvbmFsaXR5LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxrQkFBa0I7SUFDbEIsUUFBUTtJQUNSLFNBQVM7SUFDVCw4QkFBOEI7SUFDOUIsNERBQTREO0lBQzVELFlBQVk7SUFDWixpQkFBaUI7SUFDakIsbUJBQW1CO0lBQ25CLDZCQUE2QjtBQUNqQztBQUNBO0lBQ0kseUJBQXlCO0lBQ3pCLGNBQWM7SUFDZCxrQkFBa0I7SUFDbEIsb0JBQW9CO0lBQ3BCLFlBQVk7O0FBRWhCO0FBRUE7SUFDSSxVQUFVO0lBQ1YsYUFBYTtJQUNiLGFBQWE7SUFDYixXQUFXO0lBQ1gsV0FBVztJQUNYLFlBQVk7SUFDWixjQUFjO0lBQ2QsbUJBQW1COztBQUV2QjtBQUNBO0lBQ0ksV0FBVzs7QUFFZjtBQUNBO0lBQ0ksVUFBVTtJQUNWLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLFlBQVk7SUFDWixnQkFBZ0I7QUFDcEI7QUFDQTtJQUNJLDRCQUE0QjtJQUM1QixnQkFBZ0I7QUFDcEIiLCJmaWxlIjoicGVyc29uYWxpdHkuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mb3JtLWNvbnRhaW5lcntcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogNTAlO1xyXG4gICAgbGVmdDogNTAlO1xyXG4gICAgdHJhbnNmb3JtOnRyYW5zbGF0ZSgtNTAlLC01MCUpO1xyXG4gICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHJnYmEoMCwwLDAsMC4zKSxyZ2JhKDAsMCwwLDAuMykpO1xyXG4gICAgd2lkdGggOjM4MHB4O1xyXG4gICAgcGFkZGluZzo1MHB4IDMwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzIDoxMHB4O1xyXG4gICAgYm94LXNoYWRvdzogN3B4IDVweCA2MHB4ICMwMDA7XHJcbn1cclxuaDF7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgZm9udC1zaXplIDoyZW07XHJcbiAgICB0ZXh0LWFsaWduIDpjZW50ZXI7XHJcbiAgICBtYXJnaW4tYm90dG9tIDogMGVtIDsgXHJcbiAgICBjb2xvcjogd2hpdGU7XHJcblxyXG59XHJcblxyXG4uY29udHJvbCBpbnB1dHtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBkaXNwbGF5OmJsb2NrO1xyXG4gICAgcGFkZGluZyA6MTBweDtcclxuICAgIGNvbG9yOiAjMDAwO1xyXG4gICAgYm9yZGVyOm5vbmU7XHJcbiAgICBvdXRsaW5lOm5vbmU7XHJcbiAgICBtYXJnaW46IDFlbSAwIDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7IFxyXG5cclxufVxyXG4uZm9ybS1sYWJlbHtcclxuICAgIGNvbG9yOndoaXRlO1xyXG5cclxufVxyXG5idXR0b257XHJcbiAgICB3aWR0aDogNjAlO1xyXG4gICAgYmFja2dyb3VuZDogcHVycGxlO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbn1cclxuYnV0dG9uOmhvdmVye1xyXG4gICAgYmFja2dyb3VuZDogcmdiKDk0LCA5NCwgMjEyKTtcclxuICAgIHRyYW5zaXRpb246IDAuNXM7XHJcbn1cclxuIl19 */");

/***/ }),

/***/ "bBbC":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/main/main.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-navigation></app-navigation>\r\n\r\n\r\n<!-- PRE LOADER -->\r\n<section class=\"preloader\">\r\n     <div class=\"spinner\">\r\n \r\n          <span class=\"spinner-rotate\"></span>\r\n          \r\n      </div>\r\n </section>\r\n \r\n \r\n <!-- MENU -->\r\n <router-outlet></router-outlet>\r\n \r\n \r\n <!-- HOME -->\r\n <section id=\"home\">\r\n     <div class=\"row\">\r\n \r\n               <div class=\"owl-carousel owl-theme home-slider\">\r\n                    <div class=\"item item-first\">\r\n                         <div class=\"caption\">\r\n                              <div class=\"container\">\r\n                                   <div class=\"col-md-6 col-sm-12\">\r\n                                        <h1>Top Rated Online Couples Therapy & Marriage Counseling</h1>\r\n                                        <h3> Helping You Increase Trust, Intimacy, and Connection in Your Relationship</h3>\r\n                                        <a routerLink=\"/registration\" class=\"section-btn btn btn-default smoothScroll\">Get Started</a>\r\n                                   </div>\r\n                              </div>\r\n                         </div>\r\n                    </div>\r\n \r\n                    <div class=\"item item-second\">\r\n                         <div class=\"caption\">\r\n                              <div class=\"container\">\r\n                                   <div class=\"col-md-6 col-sm-12\">\r\n                                        <h1>Serious about saving or improving your relationship?</h1>\r\n                                        <h3>Get professional counseling from a licensed therapist.</h3>\r\n                                        <a routerLink=\"/registration\" class=\"section-btn btn btn-default smoothScroll\">Get Started</a>\r\n                                   </div>\r\n                              </div>\r\n                         </div>\r\n                    </div>\r\n \r\n                    <div class=\"item item-third\">\r\n                         <div class=\"caption\">\r\n                              <div class=\"container\">\r\n                                   <div class=\"col-md-6 col-sm-12\">\r\n                                        <h1>Individual and couples counseling. Anytime, anywhere.</h1>\r\n                                        <a routerLink=\"/registration\" class=\"section-btn btn btn-default smoothScroll\">Get Started</a>\r\n \r\n                                   </div>\r\n                              </div>\r\n                         </div>\r\n                    </div>\r\n               </div>\r\n     </div>\r\n </section>\r\n \r\n \r\n <!-- FEATURE -->\r\n <!-- <section id=\"feature\">\r\n     <div class=\"container\">\r\n          <div class=\"row\">\r\n \r\n               <div class=\"col-md-4 col-sm-4\">\r\n                    <div class=\"feature-thumb\">\r\n                         <span>01</span>\r\n                         <h3>Trending Courses</h3>\r\n                         <p>Known is free education HTML Bootstrap Template. You can download and use this for your website.</p>\r\n                    </div>\r\n               </div>\r\n \r\n               <div class=\"col-md-4 col-sm-4\">\r\n                    <div class=\"feature-thumb\">\r\n                         <span>02</span>\r\n                         <h3>Books & Library</h3>\r\n                         <p>Lorem ipsum dolor sit amet, consectetur adipisicing eiusmod tempor incididunt ut labore et dolore magna.</p>\r\n                    </div>\r\n               </div>\r\n \r\n               <div class=\"col-md-4 col-sm-4\">\r\n                    <div class=\"feature-thumb\">\r\n                         <span>03</span>\r\n                         <h3>Certified Teachers</h3>\r\n                         <p>templatemo provides a wide variety of free Bootstrap Templates for you. Please tell your friends about us. Thank you.</p>\r\n                    </div>\r\n               </div>\r\n \r\n          </div>\r\n      </div>\r\n </section> -->\r\n \r\n \r\n <!-- ABOUT -->\r\n <section id=\"about\">\r\n     <div class=\"container\">\r\n          <div class=\"row\">\r\n \r\n               <div class=\"col-md-6 col-sm-12\">\r\n                    <div class=\"about-info\">\r\n                         <h1>About Us</h1>\r\n                         <h4>Are you about to get married and need the strategy to prepare and build right?\r\n                             Are you about to get married and need the strategy to prepare and build right? \r\n                             Are you concerned about your marriage and need to go from coping to thriving? <br>\r\n                             We can help! Contact us today! </h4>\r\n                             <p>We are thrilled that you seek to grow in your relationships, marriage & family and we are committed to helping you achieve that goal.  \r\n \r\n                                 A Great and healthy marriage is no walk-in-the-park; it is a product of sacrifice, intentionality and commitment.  Marriage is hardwork and we recognize that the demands of marriage can be emotionally and psychologically challenging even for the prepared but more so for the unprepared. This is why we are here-to help you and your marriage thrive. \r\n                                 \r\n                                 We are licensed counselors and Certified Marriage Mentors/Relationship Coaches and we want to equip you with the tools and skills necessary in building the marriage of your dream and cultivating healthy relationships. </p>\r\n                                     </div>\r\n               </div>\r\n \r\n               <div class=\"col-md-offset-1 col-md-4 col-sm-12\">\r\n                    <h2>What We Do:</h2>\r\n                    <p> * Premarital Counseling <br>\r\n \r\n                     * Post Marital Counseling <br>\r\n                     \r\n                     * Marriage & Family Enrichment Coaching <br>\r\n                     \r\n                     * Relationship Coaching <br>\r\n                     \r\n                     * Speaking Engagements <br>\r\n                     \r\n                     Our services are mainly virtual via Zoom, Google meets etc </p>\r\n               </div>\r\n \r\n          </div>\r\n      </div>\r\n </section>\r\n \r\n \r\n <!-- Counsellors-->\r\n <section id=\"team\">\r\n     <div class=\"container\">\r\n          <div class=\"row\">\r\n \r\n               <div class=\"col-md-12 col-sm-12\">\r\n                    <div class=\"section-title\">\r\n                         <h2>For Counsellors</h2>\r\n                    </div>\r\n               </div>\r\n \r\n               <div class=\"col-md-offset-1 col-md-4 col-sm-12\">\r\n                 <div class=\"entry-form\">\r\n                      <form (submit) = \"loginUser($event)\">\r\n                <br>\r\n                <br>\r\n                   <input type=\"email\" class=\"form-control\" id=\"email\" placeholder=\"Email Address\" style=\"\r\n                   color: white;\r\n               \">\r\n                   <div id=\"emailHelp\" class=\"form-text\"></div>\r\n         \r\n                  \r\n                 \r\n                     <input type=\"password\" class=\"form-control\" id=\"password\" placeholder=\"Enter Password\" style=\"\r\n                     color: white;\r\n                 \">\r\n                     <br>\r\n                     <br>\r\n                     <button type='submit' id='submit'>Login</button>\r\n                \r\n                      </form>\r\n                 </div>\r\n            </div>\r\n \r\n \r\n          </div>\r\n     </div>\r\n </section>\r\n \r\n \r\n <section id=\"courses\">\r\n     <div class=\"container\">\r\n          <div class=\"row\">\r\n               <div class=\"col-6\">\r\n              <h2>Statement Of Faith</h2>\r\n             <p>I appeal to you, brothers and sisters, in the name of our Lord Jesus Christ, that all of you agree with one another in what you say and that there be no divisions among you, but that you be perfectly united in mind and thought.\" 1 Corinthians 1:10\r\n \r\n                 Paul's message for the Corinthian church calls us to a very high standard. When we observe the many denominations and theological rifts within the Christian church today, it can seem a daunting task to be \"perfectly united in mind and thought.\r\n                 \" Despite this, we believe that these differences in thought that exist in the church needn't outweigh the greater truths that bind us together, and it is important that all members of Marriage & Relastionship Counseling--counselors and members alike--make an effort to strive toward being united in Christ under what matters most.\r\n                  We believe this will lead to the most successful counseling sessions, and any further details around theological leanings needn't be pressed. In order to reach common ground on what we believe are the most fundamental truths, all counseling members of our platform have agreed to the following core tenets of faith: <br>\r\n                 1. We believe that the Bible is the word of God, a collection of divinely-inspired writings that have been preserved for each generation, inspired by the Holy Spirit, and the only authoritative and infallible rule of Christian faith and practice. (\"All Scripture is breathed out by God and profitable for teaching, for reproof, for correction, and for training in righteousness, that the man of God may be complete, equipped for every good work.\" 2 Timothy 3:16-17 ESV) <br>\r\n                 2. We believe in the Triune nature of God (Trinitarianism), that He is Father, Son, and Holy Spirit in one. (\"Go therefore and make disciples of all nations, baptizing them in the name of the Father and of the Son and of the Holy Spirit, teaching them to observe all that I have commanded you. And behold, I am with you always, to the end of the age.\" Matthew 28:19-20 ESV) <br>\r\n                 3. We believe the only true basis of Christian fellowship is Christ's (agape) love, which is greater than differences one may possess, and without which we have no right to claim ourselves Christians. (\"Jesus said to him, \"I am the way, and the truth, and the life. No one comes to the Father except through me.\" John 14:6 ESV) <br>\r\n                 4. We believe salvation is by God's grace through faith in Jesus Christ who died for our sins and rose again, providing eternal redemption to those who believe. It is not by our works or works of the law. (\"For by grace you have been saved through faith. And this is not your own doing; it is the gift of God, not a result of works, so that no one may boast.\" Ephesians 2:8-9 ESV)</p>\r\n               </div>\r\n \r\n       </div>\r\n     </div>\r\n         \r\n \r\n </section>\r\n \r\n <!-- TESTIMONIAL -->\r\n <section id=\"testimonial\">\r\n     <div class=\"container\">\r\n          <div class=\"row\">\r\n               <div class=\"col-md-12 col-sm-12\">\r\n                    <div class=\"section-title\">\r\n                         <h2>Couple Reviews <small>from around the world</small></h2>\r\n                    </div>\r\n \r\n                    <div class=\"owl-carousel owl-theme owl-client\">\r\n                         <div class=\"col-md-4 col-sm-4\">\r\n                              <div class=\"item\">\r\n                                   <div class=\"tst-image\">\r\n                                        <img src=\"assets/images/tst-image1.jpg\" class=\"img-responsive\" alt=\"\">\r\n                                   </div>\r\n                                   <div class=\"tst-author\">\r\n                                        <h4>Jackson</h4>\r\n                                        <span>Shopify Developer</span>\r\n                                   </div>\r\n                                   <p>You really do help young creative minds to get quality education and professional job search assistance. I’d recommend it to everyone!</p>\r\n                                   <div class=\"tst-rating\">\r\n                                        <i class=\"fa fa-star\"></i>\r\n                                        <i class=\"fa fa-star\"></i>\r\n                                        <i class=\"fa fa-star\"></i>\r\n                                        <i class=\"fa fa-star\"></i>\r\n                                        <i class=\"fa fa-star\"></i>\r\n                                   </div>\r\n                              </div>\r\n                         </div>\r\n \r\n                         <div class=\"col-md-4 col-sm-4\">\r\n                              <div class=\"item\">\r\n                                   <div class=\"tst-image\">\r\n                                        <img src=\"assets/images/tst-image2.jpg\" class=\"img-responsive\" alt=\"\">\r\n                                   </div>\r\n                                   <div class=\"tst-author\">\r\n                                        <h4>Camila</h4>\r\n                                        <span>Marketing Manager</span>\r\n                                   </div>\r\n                                   <p>Trying something new is exciting! Thanks for the amazing law course and the great teacher who was able to make it interesting.</p>\r\n                                   <div class=\"tst-rating\">\r\n                                        <i class=\"fa fa-star\"></i>\r\n                                        <i class=\"fa fa-star\"></i>\r\n                                        <i class=\"fa fa-star\"></i>\r\n                                   </div>\r\n                              </div>\r\n                         </div>\r\n \r\n                         <div class=\"col-md-4 col-sm-4\">\r\n                              <div class=\"item\">\r\n                                   <div class=\"tst-image\">\r\n                                        <img src=\"assets/images/tst-image3.jpg\" class=\"img-responsive\" alt=\"\">\r\n                                   </div>\r\n                                   <div class=\"tst-author\">\r\n                                        <h4>Barbie</h4>\r\n                                        <span>Art Director</span>\r\n                                   </div>\r\n                                   <p>Donec erat libero, blandit vitae arcu eu, lacinia placerat justo. Sed sollicitudin quis felis vitae hendrerit.</p>\r\n                                   <div class=\"tst-rating\">\r\n                                        <i class=\"fa fa-star\"></i>\r\n                                        <i class=\"fa fa-star\"></i>\r\n                                        <i class=\"fa fa-star\"></i>\r\n                                        <i class=\"fa fa-star\"></i>\r\n                                   </div>\r\n                              </div>\r\n                         </div>\r\n \r\n                         <div class=\"col-md-4 col-sm-4\">\r\n                              <div class=\"item\">\r\n                                   <div class=\"tst-image\">\r\n                                        <img src=\"assets/images/tst-image4.jpg\" class=\"img-responsive\" alt=\"\">\r\n                                   </div>\r\n                                   <div class=\"tst-author\">\r\n                                        <h4>Andrio</h4>\r\n                                        <span>Web Developer</span>\r\n                                   </div>\r\n                                   <p>Nam eget mi eu ante faucibus viverra nec sed magna. Vivamus viverra sapien ex, elementum varius ex sagittis vel.</p>\r\n                                   <div class=\"tst-rating\">\r\n                                        <i class=\"fa fa-star\"></i>\r\n                                        <i class=\"fa fa-star\"></i>\r\n                                        <i class=\"fa fa-star\"></i>\r\n                                        <i class=\"fa fa-star\"></i>\r\n                                   </div>\r\n                              </div>\r\n                         </div>\r\n \r\n                    </div>\r\n                </div>\r\n           </div>\r\n     </div>\r\n </section> \r\n \r\n \r\n <!-- CONTACT -->\r\n <section id=\"contact\">\r\n     <div class=\"container\">\r\n          <div class=\"row\">\r\n \r\n               <div class=\"col-md-6 col-sm-12\">\r\n                    <form id=\"contact-form\" role=\"form\" action=\"\" method=\"post\">\r\n                         <div class=\"section-title\">\r\n                              <h2>Contact us <small>we love conversations. let us talk!</small></h2>\r\n                         </div>\r\n \r\n                         <div class=\"col-md-12 col-sm-12\">\r\n                              <input type=\"text\" class=\"form-control\" placeholder=\"Enter full name\" name=\"name\" required=\"\">\r\n               \r\n                              <input type=\"email\" class=\"form-control\" placeholder=\"Enter email address\" name=\"email\" required=\"\">\r\n \r\n                              <textarea class=\"form-control\" rows=\"6\" placeholder=\"Tell us about your message\" name=\"message\" required=\"\"></textarea>\r\n                         </div>\r\n \r\n                         <div class=\"col-md-4 col-sm-12\">\r\n                              <input type=\"submit\" class=\"form-control\" name=\"send message\" value=\"Send Message\">\r\n                         </div>\r\n \r\n                    </form>\r\n               </div>\r\n \r\n               <div class=\"col-md-6 col-sm-12\">\r\n                    <div class=\"contact-image\">\r\n                         <img src=\"assets/images/contact-image.jpg\" class=\"img-responsive\" alt=\"Smiling Two Girls\">\r\n                    </div>\r\n               </div>\r\n \r\n          </div>\r\n     </div>\r\n </section>       \r\n \r\n \r\n <!-- FOOTER -->\r\n <footer id=\"footer\">\r\n     <div class=\"container\">\r\n          <div class=\"row\">\r\n \r\n               <div class=\"col-md-4 col-sm-6\">\r\n                    <div class=\"footer-info\">\r\n                         <div class=\"section-title\">\r\n                              <h2>Headquarter</h2>\r\n                         </div>\r\n                         <address>\r\n                              <p>1800 ***** a **** ****,<br> ***** a **** **** 12000</p>\r\n                         </address>\r\n \r\n                         <ul class=\"social-icon\">\r\n                              <li><a href=\"#\" class=\"fa fa-facebook-square\" attr=\"facebook icon\"></a></li>\r\n                              <li><a href=\"#\" class=\"fa fa-twitter\"></a></li>\r\n                              <li><a href=\"#\" class=\"fa fa-instagram\"></a></li>\r\n                         </ul>\r\n \r\n                         <div class=\"copyright-text\"> \r\n                              <p>Copyright &copy; 2021 Marriage & Relationship Kitchen</p>    \r\n                         </div>\r\n                    </div>\r\n               </div>\r\n \r\n               <div class=\"col-md-4 col-sm-6\">\r\n                    <div class=\"footer-info\">\r\n                         <div class=\"section-title\">\r\n                              <h2>Contact Info</h2>\r\n                         </div>\r\n                         <address>\r\n                              <p>+** **** ****, +** **** ****</p>\r\n                              <p><a href=\"mailto:youremail.co\">hello@youremail.co</a></p>\r\n                         </address>\r\n \r\n                         <div class=\"footer_menu\">\r\n                              <h2>Quick Links</h2>\r\n                              <ul>\r\n                                   <li><a href=\"#\">Career</a></li>\r\n                                   <li><a href=\"#\">Investor</a></li>\r\n                                   <li><a href=\"#\">Terms & Conditions</a></li>\r\n                                   <li><a href=\"#\">Refund Policy</a></li>\r\n                              </ul>\r\n                         </div>\r\n                    </div>\r\n               </div>\r\n \r\n               <div class=\"col-md-4 col-sm-12\">\r\n                    <div class=\"footer-info newsletter-form\">\r\n                         <div class=\"section-title\">\r\n                              <h2>Newsletter Signup</h2>\r\n                         </div>\r\n                         <div>\r\n                              <div class=\"form-group\">\r\n                                   <form action=\"#\" method=\"get\">\r\n                                        <input type=\"email\" class=\"form-control\" placeholder=\"Enter your email\" name=\"email\" id=\"email\" required=\"\">\r\n                                        <input type=\"submit\" class=\"form-control\" name=\"submit\" id=\"form-submit\" value=\"Send me\">\r\n                                   </form>\r\n                                   <span><sup>*</sup> Please note - we do not spam your email.</span>\r\n                              </div>\r\n                         </div>\r\n                    </div>\r\n               </div>\r\n               \r\n          </div>\r\n     </div>\r\n </footer>\r\n \r\n\r\n");

/***/ }),

/***/ "cmq7":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/main/verification/verification.component.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n<header id=\"banner\" class=\"scrollto clearfix\" data-enllax-ratio=\".5\">\r\n    <div class=\"form-container\">\r\n        <h1 style=\"color:white; font-size:42px\">OTP Verification</h1>\r\n            <form (submit) = \"getOTP($event)\">\r\n                <div class=\"control\">\r\n                 <p style=\"color:white\">You will Recieve An OTP To Continue</p>\r\n                 <input type=\"otp\" class=\"form-control\" id=\"otp\">\r\n            \r\n                </div>\r\n            <p class=\"text-center\">\r\n              <button type='submit' id='submit'>Login</button>\r\n                 \r\n         </p>\r\n              </form>\r\n    </div>\r\n    \r\n    </header>");

/***/ }),

/***/ "jhpP":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/main/personality/personality.component.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n<header id=\"banner\" class=\"scrollto clearfix\" data-enllax-ratio=\".5\">\r\n    <div class=\"form-container\">\r\n        <h1>Login Form</h1>\r\n            <form (submit) = \"loginUser($event)\">\r\n                <div class=\"control\">\r\n                  <label for=\"exampleInputEmail\" class=\"form-label\">Email Adress</label>\r\n                  <input type=\"email\" class=\"form-control\" id=\"email\">\r\n                  <div id=\"emailHelp\" class=\"form-text\"></div>\r\n                </div>\r\n                  <div class=\"control\">\r\n                    <label for=\"exampleInputPassword1\" class=\"form-label\">Password</label>\r\n                    <input type=\"password\" class=\"form-control\" id=\"password\">\r\n                  </div>\r\n            <p class=\"text-center\">\r\n              <button type='submit' id='submit'>Login</button>\r\n              <!-- <button routerLink=\"/verification\"> Login</button> -->\r\n                  </p>\r\n              </form>\r\n    </div>\r\n    \r\n    </header>");

/***/ }),

/***/ "lSLK":
/*!*****************************************************************!*\
  !*** ./src/app/pages/main/personality/personality.component.ts ***!
  \*****************************************************************/
/*! exports provided: PersonalityComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PersonalityComponent", function() { return PersonalityComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_personality_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./personality.component.html */ "jhpP");
/* harmony import */ var _personality_component_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./personality.component.css */ "N3D8");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngxs/store */ "e1JD");
/* harmony import */ var src_app_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/auth.service */ "ccyI");
/* harmony import */ var src_app_http_http_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/http/http.service */ "RjLs");
/* harmony import */ var src_app_store_store_actions__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/store/store.actions */ "kPKD");









let PersonalityComponent = class PersonalityComponent {
    constructor(Auth, router, http, store) {
        this.Auth = Auth;
        this.router = router;
        this.http = http;
        this.store = store;
    }
    ngOnInit() {
    }
    loginUser(event) {
        event.preventDefault();
        const target = event.target;
        const email = target.querySelector("#email").value;
        const password = target.querySelector("#password").value;
        if (email !== 'admin@gmail.com' && password !== '@admin12') {
            return;
        }
        return this.store.dispatch(new src_app_store_store_actions__WEBPACK_IMPORTED_MODULE_8__["PushState"]({
            admin: {
                email: email,
                password: password
            }
        })).subscribe(() => {
            console.log('Hello...');
            this.router.navigate(['dashboard']);
        });
    }
};
PersonalityComponent.ctorParameters = () => [
    { type: src_app_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_http_http_service__WEBPACK_IMPORTED_MODULE_7__["HttpService"] },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Store"] }
];
PersonalityComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-personality',
        template: _raw_loader_personality_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_personality_component_css__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], PersonalityComponent);



/***/ }),

/***/ "piB7":
/*!***********************************************!*\
  !*** ./src/app/pages/main/main.component.css ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("h4{\r\n    text-align:justify ;\r\n}\r\n\r\np{\r\n    text-align: justify;\r\n    margin-left: 10px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLG1CQUFtQjtBQUN2Qjs7QUFFQTtJQUNJLG1CQUFtQjtJQUNuQixpQkFBaUI7QUFDckIiLCJmaWxlIjoibWFpbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaDR7XHJcbiAgICB0ZXh0LWFsaWduOmp1c3RpZnkgO1xyXG59XHJcblxyXG5we1xyXG4gICAgdGV4dC1hbGlnbjoganVzdGlmeTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG59Il19 */");

/***/ }),

/***/ "ppKA":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/main/login/login.component.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("");

/***/ }),

/***/ "tz80":
/*!*******************************************************************!*\
  !*** ./src/app/pages/main/verification/verification.component.ts ***!
  \*******************************************************************/
/*! exports provided: VerificationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerificationComponent", function() { return VerificationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_verification_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./verification.component.html */ "cmq7");
/* harmony import */ var _verification_component_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./verification.component.css */ "wj3g");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var src_app_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/auth.service */ "ccyI");






let VerificationComponent = class VerificationComponent {
    constructor(Auth, router) {
        this.Auth = Auth;
        this.router = router;
    }
    ngOnInit() {
    }
    getOTP(event) {
        event.preventDefault();
        const target = event.target;
        const otp = target.querySelector("#otp").value;
        this.Auth.getOTP(otp).subscribe(data => {
            if (data.success) {
                this.router.navigate(['personality-test']);
            }
            else {
                window.alert("Wrong OTP");
            }
        });
    }
};
VerificationComponent.ctorParameters = () => [
    { type: src_app_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
VerificationComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-verification',
        template: _raw_loader_verification_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_verification_component_css__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], VerificationComponent);



/***/ }),

/***/ "wj3g":
/*!********************************************************************!*\
  !*** ./src/app/pages/main/verification/verification.component.css ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".form-container{\r\n    position: absolute;\r\n    top: 50%;\r\n    left: 50%;\r\n    transform:translate(-50%,-50%);\r\n    background: linear-gradient(rgba(0,0,0,0.3),rgba(0,0,0,0.3));\r\n    width :380px;\r\n    padding:50px 30px;\r\n    border-radius :10px;\r\n    box-shadow: 7px 5px 60px #000;\r\n}\r\nh1{\r\n    text-transform: uppercase;\r\n    font-size :2em;\r\n    text-align :center;\r\n    margin-bottom : 0em ; \r\n    color: white;\r\n\r\n}\r\n.control input{\r\n    width:100%;\r\n    display:block;\r\n    padding :10px;\r\n    color: #000;\r\n    border:none;\r\n    outline:none;\r\n    margin: 1em 0 ;\r\n    border-radius: 20px; \r\n\r\n}\r\n.form-label{\r\n    color:white;\r\n\r\n}\r\n.submit{\r\n    float: right;\r\n    margin-top: 40px;\r\n    border-radius: 20px 20px 20px 20px;\r\n    border-color: aliceblue;\r\n    height: 30px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInZlcmlmaWNhdGlvbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksa0JBQWtCO0lBQ2xCLFFBQVE7SUFDUixTQUFTO0lBQ1QsOEJBQThCO0lBQzlCLDREQUE0RDtJQUM1RCxZQUFZO0lBQ1osaUJBQWlCO0lBQ2pCLG1CQUFtQjtJQUNuQiw2QkFBNkI7QUFDakM7QUFDQTtJQUNJLHlCQUF5QjtJQUN6QixjQUFjO0lBQ2Qsa0JBQWtCO0lBQ2xCLG9CQUFvQjtJQUNwQixZQUFZOztBQUVoQjtBQUVBO0lBQ0ksVUFBVTtJQUNWLGFBQWE7SUFDYixhQUFhO0lBQ2IsV0FBVztJQUNYLFdBQVc7SUFDWCxZQUFZO0lBQ1osY0FBYztJQUNkLG1CQUFtQjs7QUFFdkI7QUFDQTtJQUNJLFdBQVc7O0FBRWY7QUFDQTtJQUNJLFlBQVk7SUFDWixnQkFBZ0I7SUFDaEIsa0NBQWtDO0lBQ2xDLHVCQUF1QjtJQUN2QixZQUFZO0FBQ2hCIiwiZmlsZSI6InZlcmlmaWNhdGlvbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZvcm0tY29udGFpbmVye1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiA1MCU7XHJcbiAgICBsZWZ0OiA1MCU7XHJcbiAgICB0cmFuc2Zvcm06dHJhbnNsYXRlKC01MCUsLTUwJSk7XHJcbiAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQocmdiYSgwLDAsMCwwLjMpLHJnYmEoMCwwLDAsMC4zKSk7XHJcbiAgICB3aWR0aCA6MzgwcHg7XHJcbiAgICBwYWRkaW5nOjUwcHggMzBweDtcclxuICAgIGJvcmRlci1yYWRpdXMgOjEwcHg7XHJcbiAgICBib3gtc2hhZG93OiA3cHggNXB4IDYwcHggIzAwMDtcclxufVxyXG5oMXtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBmb250LXNpemUgOjJlbTtcclxuICAgIHRleHQtYWxpZ24gOmNlbnRlcjtcclxuICAgIG1hcmdpbi1ib3R0b20gOiAwZW0gOyBcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuXHJcbn1cclxuXHJcbi5jb250cm9sIGlucHV0e1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIGRpc3BsYXk6YmxvY2s7XHJcbiAgICBwYWRkaW5nIDoxMHB4O1xyXG4gICAgY29sb3I6ICMwMDA7XHJcbiAgICBib3JkZXI6bm9uZTtcclxuICAgIG91dGxpbmU6bm9uZTtcclxuICAgIG1hcmdpbjogMWVtIDAgO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjBweDsgXHJcblxyXG59XHJcbi5mb3JtLWxhYmVse1xyXG4gICAgY29sb3I6d2hpdGU7XHJcblxyXG59XHJcbi5zdWJtaXR7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICBtYXJnaW4tdG9wOiA0MHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjBweCAyMHB4IDIwcHggMjBweDtcclxuICAgIGJvcmRlci1jb2xvcjogYWxpY2VibHVlO1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG59Il19 */");

/***/ })

}]);
//# sourceMappingURL=pages-main-main-module.js.map