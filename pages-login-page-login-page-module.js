(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-page-login-page-module"],{

/***/ "6V8c":
/*!**********************************************************!*\
  !*** ./src/app/pages/login-page/login-page.component.ts ***!
  \**********************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_login_page_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./login-page.component.html */ "dgLM");
/* harmony import */ var _login_page_component_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login-page.component.css */ "WfqL");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var src_app_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/auth.service */ "ccyI");
/* harmony import */ var src_app_http_http_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/http/http.service */ "RjLs");







let LoginComponent = class LoginComponent {
    constructor(Auth, router, http) {
        this.Auth = Auth;
        this.router = router;
        this.http = http;
    }
    ngOnInit() {
    }
    loginUser1(event) {
        event.preventDefault();
        const target = event.target;
        const email = target.querySelector("#email").value;
        const password = target.querySelector("#password").value;
        this.Auth.getUserDetails(email, password).subscribe(data => {
            if (data.success) {
                this.router.navigate(['Home/verification']);
                this.Auth.setLoggedIn(true);
            }
        });
    }
};
LoginComponent.ctorParameters = () => [
    { type: src_app_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_http_http_service__WEBPACK_IMPORTED_MODULE_6__["HttpService"] }
];
LoginComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-login-page',
        template: _raw_loader_login_page_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_login_page_component_css__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], LoginComponent);



/***/ }),

/***/ "Rr7R":
/*!*******************************************************!*\
  !*** ./src/app/pages/login-page/login-page.module.ts ***!
  \*******************************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "s7LF");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var _login_page_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./login-page.component */ "6V8c");





const routes = [
    {
        path: '',
        component: _login_page_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"]
    }
];
let LoginPageModule = class LoginPageModule {
};
LoginPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _login_page_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"]
        ],
        imports: [
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
        ],
        exports: []
    })
], LoginPageModule);

;


/***/ }),

/***/ "WfqL":
/*!***********************************************************!*\
  !*** ./src/app/pages/login-page/login-page.component.css ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("*{\r\n    margin: 0;\r\n    padding: 0;\r\n    box-sizing: border-box;\r\n    font-family: 'Poppins',sans-serif;\r\n  }\r\n  body{\r\n    display: grid;\r\n    height: 100vh;\r\n    place-items:center;\r\n    background-image:url('background.jpg');\r\n    background-size: cover;\r\n    background-repeat: no-repeat;\r\n  }\r\n  .main_div{\r\n    width: 365px;\r\n    background: #fff;\r\n    padding: 30px;\r\n    border-radius: 5px;\r\n    box-shadow: 5px 5px 10px rgba(0, 0, 0, 0.15);\r\n    margin: auto;\r\n    opacity: 0.9;\r\n  }\r\n  .main_div .title{\r\n    text-align: center;\r\n    font-size: 30px;\r\n    font-weight: 600;\r\n  }\r\n  .main_div .social_icons{\r\n    margin-top: 20px;\r\n    display: flex;\r\n    justify-content: center;\r\n  }\r\n  .social_icons a{\r\n    display: block;\r\n    height: 45px;\r\n    width: 100%;\r\n    line-height: 45px;\r\n    text-align: center;\r\n    border-radius: 5px;\r\n    font-size: 20px;\r\n    color: #fff;\r\n    text-decoration: none;\r\n    transition: all 0.3s linear;\r\n  }\r\n  .social_icons a span{\r\n    margin-left: 5px;\r\n    font-size: 18px;\r\n  }\r\n  .social_icons a:first-child{\r\n    margin-right: 5px;\r\n    background: #4267B2;\r\n  }\r\n  .social_icons a:first-child:hover{\r\n    background: #375695;\r\n  }\r\n  .social_icons a:last-child{\r\n    margin-left: 5px;\r\n    background: #1DA1F2;\r\n  }\r\n  .social_icons a:last-child:hover{\r\n    background: #0d8bd9;\r\n  }\r\n  form {\r\n    margin-top: 25px;\r\n  }\r\n  form .input_box{\r\n    height: 50px;\r\n    width: 100%;\r\n    position: relative;\r\n    margin-top: 15px;\r\n  }\r\n  .input_box input{\r\n    height: 100%;\r\n    width: 100%;\r\n    outline: none;\r\n    border: 1px solid lightgrey;\r\n    border-radius: 5px;\r\n    padding-left: 45px;\r\n    font-size: 17px;\r\n    transition: all 0.3s ease;\r\n  }\r\n  .input_box input:focus{\r\n    border-color: #be2edd;\r\n  }\r\n  .input_box .icon{\r\n    position: absolute;\r\n    top: 50%;\r\n    left: 20px;\r\n    transform: translateY(-50%);\r\n    color: grey;\r\n  }\r\n  form .option_div{\r\n    margin-top: 5px;\r\n    display: flex;\r\n    justify-content: space-between;\r\n  }\r\n  .option_div .check_box{\r\n    display: flex;\r\n    align-items: center;\r\n  }\r\n  .option_div span{\r\n    margin-left: 5px;\r\n    font-size: 16px;\r\n    color: #333;\r\n  }\r\n  .option_div .forget_div a{\r\n    font-size: 16px;\r\n    color: #be2edd;\r\n  }\r\n  .button input{\r\n    padding-left: 0;\r\n    background: #be2edd;\r\n    color: #fff;\r\n    border: none;\r\n    font-size: 20px;\r\n    font-weight: 500;\r\n    cursor: pointer;\r\n    transition: all 0.3s linear;\r\n  }\r\n  .button input:hover{\r\n    background: #a720c5;\r\n  }\r\n  form .sign_up{\r\n    text-align: center;\r\n    margin-top: 25px;\r\n  }\r\n  .sign_up a{\r\n    color: #be2edd;\r\n  }\r\n  form a{\r\n    text-decoration: none;\r\n  }\r\n  form a:hover{\r\n    text-decoration: underline;\r\n  }\r\n  \r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvZ2luLXBhZ2UuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFNBQVM7SUFDVCxVQUFVO0lBQ1Ysc0JBQXNCO0lBQ3RCLGlDQUFpQztFQUNuQztFQUNBO0lBQ0UsYUFBYTtJQUNiLGFBQWE7SUFDYixrQkFBa0I7SUFDbEIsc0NBQXdEO0lBQ3hELHNCQUFzQjtJQUN0Qiw0QkFBNEI7RUFDOUI7RUFDQTtJQUNFLFlBQVk7SUFDWixnQkFBZ0I7SUFDaEIsYUFBYTtJQUNiLGtCQUFrQjtJQUNsQiw0Q0FBNEM7SUFDNUMsWUFBWTtJQUNaLFlBQVk7RUFDZDtFQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixnQkFBZ0I7RUFDbEI7RUFDQTtJQUNFLGdCQUFnQjtJQUNoQixhQUFhO0lBQ2IsdUJBQXVCO0VBQ3pCO0VBQ0E7SUFDRSxjQUFjO0lBQ2QsWUFBWTtJQUNaLFdBQVc7SUFDWCxpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsV0FBVztJQUNYLHFCQUFxQjtJQUNyQiwyQkFBMkI7RUFDN0I7RUFDQTtJQUNFLGdCQUFnQjtJQUNoQixlQUFlO0VBQ2pCO0VBQ0E7SUFDRSxpQkFBaUI7SUFDakIsbUJBQW1CO0VBQ3JCO0VBQ0E7SUFDRSxtQkFBbUI7RUFDckI7RUFDQTtJQUNFLGdCQUFnQjtJQUNoQixtQkFBbUI7RUFDckI7RUFDQTtJQUNFLG1CQUFtQjtFQUNyQjtFQUNBO0lBQ0UsZ0JBQWdCO0VBQ2xCO0VBQ0E7SUFDRSxZQUFZO0lBQ1osV0FBVztJQUNYLGtCQUFrQjtJQUNsQixnQkFBZ0I7RUFDbEI7RUFDQTtJQUNFLFlBQVk7SUFDWixXQUFXO0lBQ1gsYUFBYTtJQUNiLDJCQUEyQjtJQUMzQixrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZix5QkFBeUI7RUFDM0I7RUFDQTtJQUNFLHFCQUFxQjtFQUN2QjtFQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLFFBQVE7SUFDUixVQUFVO0lBQ1YsMkJBQTJCO0lBQzNCLFdBQVc7RUFDYjtFQUNBO0lBQ0UsZUFBZTtJQUNmLGFBQWE7SUFDYiw4QkFBOEI7RUFDaEM7RUFDQTtJQUNFLGFBQWE7SUFDYixtQkFBbUI7RUFDckI7RUFDQTtJQUNFLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsV0FBVztFQUNiO0VBQ0E7SUFDRSxlQUFlO0lBQ2YsY0FBYztFQUNoQjtFQUNBO0lBQ0UsZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsWUFBWTtJQUNaLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLDJCQUEyQjtFQUM3QjtFQUNBO0lBQ0UsbUJBQW1CO0VBQ3JCO0VBQ0E7SUFDRSxrQkFBa0I7SUFDbEIsZ0JBQWdCO0VBQ2xCO0VBQ0E7SUFDRSxjQUFjO0VBQ2hCO0VBQ0E7SUFDRSxxQkFBcUI7RUFDdkI7RUFDQTtJQUNFLDBCQUEwQjtFQUM1QiIsImZpbGUiOiJsb2dpbi1wYWdlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIqe1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICBmb250LWZhbWlseTogJ1BvcHBpbnMnLHNhbnMtc2VyaWY7XHJcbiAgfVxyXG4gIGJvZHl7XHJcbiAgICBkaXNwbGF5OiBncmlkO1xyXG4gICAgaGVpZ2h0OiAxMDB2aDtcclxuICAgIHBsYWNlLWl0ZW1zOmNlbnRlcjtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6dXJsKFwic3JjL2Fzc2V0cy9pbWFnZXMvYmFja2dyb3VuZC5qcGdcIik7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICB9XHJcbiAgLm1haW5fZGl2e1xyXG4gICAgd2lkdGg6IDM2NXB4O1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgIHBhZGRpbmc6IDMwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBib3gtc2hhZG93OiA1cHggNXB4IDEwcHggcmdiYSgwLCAwLCAwLCAwLjE1KTtcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIG9wYWNpdHk6IDAuOTtcclxuICB9XHJcbiAgLm1haW5fZGl2IC50aXRsZXtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgfVxyXG4gIC5tYWluX2RpdiAuc29jaWFsX2ljb25ze1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICB9XHJcbiAgLnNvY2lhbF9pY29ucyBhe1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBoZWlnaHQ6IDQ1cHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGxpbmUtaGVpZ2h0OiA0NXB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC4zcyBsaW5lYXI7XHJcbiAgfVxyXG4gIC5zb2NpYWxfaWNvbnMgYSBzcGFue1xyXG4gICAgbWFyZ2luLWxlZnQ6IDVweDtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICB9XHJcbiAgLnNvY2lhbF9pY29ucyBhOmZpcnN0LWNoaWxke1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjNDI2N0IyO1xyXG4gIH1cclxuICAuc29jaWFsX2ljb25zIGE6Zmlyc3QtY2hpbGQ6aG92ZXJ7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMzc1Njk1O1xyXG4gIH1cclxuICAuc29jaWFsX2ljb25zIGE6bGFzdC1jaGlsZHtcclxuICAgIG1hcmdpbi1sZWZ0OiA1cHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMURBMUYyO1xyXG4gIH1cclxuICAuc29jaWFsX2ljb25zIGE6bGFzdC1jaGlsZDpob3ZlcntcclxuICAgIGJhY2tncm91bmQ6ICMwZDhiZDk7XHJcbiAgfVxyXG4gIGZvcm0ge1xyXG4gICAgbWFyZ2luLXRvcDogMjVweDtcclxuICB9XHJcbiAgZm9ybSAuaW5wdXRfYm94e1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBtYXJnaW4tdG9wOiAxNXB4O1xyXG4gIH1cclxuICAuaW5wdXRfYm94IGlucHV0e1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgbGlnaHRncmV5O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgcGFkZGluZy1sZWZ0OiA0NXB4O1xyXG4gICAgZm9udC1zaXplOiAxN3B4O1xyXG4gICAgdHJhbnNpdGlvbjogYWxsIDAuM3MgZWFzZTtcclxuICB9XHJcbiAgLmlucHV0X2JveCBpbnB1dDpmb2N1c3tcclxuICAgIGJvcmRlci1jb2xvcjogI2JlMmVkZDtcclxuICB9XHJcbiAgLmlucHV0X2JveCAuaWNvbntcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogNTAlO1xyXG4gICAgbGVmdDogMjBweDtcclxuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcclxuICAgIGNvbG9yOiBncmV5O1xyXG4gIH1cclxuICBmb3JtIC5vcHRpb25fZGl2e1xyXG4gICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICB9XHJcbiAgLm9wdGlvbl9kaXYgLmNoZWNrX2JveHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIH1cclxuICAub3B0aW9uX2RpdiBzcGFue1xyXG4gICAgbWFyZ2luLWxlZnQ6IDVweDtcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgIGNvbG9yOiAjMzMzO1xyXG4gIH1cclxuICAub3B0aW9uX2RpdiAuZm9yZ2V0X2RpdiBhe1xyXG4gICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgY29sb3I6ICNiZTJlZGQ7XHJcbiAgfVxyXG4gIC5idXR0b24gaW5wdXR7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDA7XHJcbiAgICBiYWNrZ3JvdW5kOiAjYmUyZWRkO1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgdHJhbnNpdGlvbjogYWxsIDAuM3MgbGluZWFyO1xyXG4gIH1cclxuICAuYnV0dG9uIGlucHV0OmhvdmVye1xyXG4gICAgYmFja2dyb3VuZDogI2E3MjBjNTtcclxuICB9XHJcbiAgZm9ybSAuc2lnbl91cHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbi10b3A6IDI1cHg7XHJcbiAgfVxyXG4gIC5zaWduX3VwIGF7XHJcbiAgICBjb2xvcjogI2JlMmVkZDtcclxuICB9XHJcbiAgZm9ybSBhe1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gIH1cclxuICBmb3JtIGE6aG92ZXJ7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcclxuICB9XHJcbiAgIl19 */");

/***/ }),

/***/ "ccyI":
/*!*********************************!*\
  !*** ./src/app/auth.service.ts ***!
  \*********************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "IheW");
/* harmony import */ var _api_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./api/api */ "lL+R");




let AuthService = class AuthService {
    constructor(http) {
        this.http = http;
        this.loggedInStatus = false;
        this.registerStatus = false;
    }
    setLoggedIn(value) {
        this.loggedInStatus = value;
    }
    get isLoggedIn() {
        return this.loggedInStatus;
    }
    setRegister(value) {
        this.registerStatus = value;
    }
    get isRegister() {
        return this.registerStatus;
    }
    getUserDetails(email, password) {
        return this.http.post(`${_api_api__WEBPACK_IMPORTED_MODULE_3__["default"]}/login`, {
            email: email,
            password: password
        });
    }
    registerUser(data) {
        return this.http.post(`${_api_api__WEBPACK_IMPORTED_MODULE_3__["default"]}/register`, data);
    }
    ;
    getOTP(otp) {
        return this.http.post(`${_api_api__WEBPACK_IMPORTED_MODULE_3__["default"]}/verify`, {
            otp: otp
        });
    }
    summary() {
        return this.http.get(`${_api_api__WEBPACK_IMPORTED_MODULE_3__["default"]}/summary/get`);
    }
};
AuthService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
AuthService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], AuthService);

;


/***/ }),

/***/ "dgLM":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login-page/login-page.component.html ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<body>\n<div class=\"main_div\">\n  <div class=\"title\">Login Form</div>\n  <form (submit) = \"loginUser1($event)\">\n    <div class=\"input_box\">\n      <input type=\"text\" placeholder=\"Email or Phone\" id=\"email\">\n    \n    </div>\n    <div class=\"input_box\">\n      <input type=\"password\" placeholder=\"Password\" id=\"password\">\n    \n    </div>\n    <div class=\"option_div\">\n      <div class=\"check_box\">\n        <input type=\"checkbox\">\n        <br>\n        <br>\n        <br>\n        <span>Remember me</span>\n      </div>\n      <div class=\"forget_div\">\n        <br>\n        <a href=\"#\">Forgot password?</a>\n      </div>\n    </div>\n    <div class=\"input_box button\">\n      <input type=\"submit\" value=\"Login\">\n    </div>\n    <div class=\"sign_up\">\n      Not a member? <a routerLink=\"/registration\">Signup now</a>\n    </div>\n  </form>\n</div>\n</body>");

/***/ })

}]);
//# sourceMappingURL=pages-login-page-login-page-module.js.map